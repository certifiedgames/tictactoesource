import java.util.Scanner;

public class TicTacToeSource{
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    char[][] Board = {
      {' ', ' ', ' '},
      {' ', ' ', ' '},
      {' ', ' ', ' '}
    };

    char WINNER = 'T';
    char playerTurn = 'X';
    int placeRow = 0;
    int placeColumn = 0;

    for (int outCount = 0; outCount < 9; outCount++){
      // UNDERSCORES ON BOARD
      for (int outI = 0; outI < 3; outI++){
        for (int inI = 0; inI < 3; inI++){
          if (Board[outI][inI] == ' '){
            Board[outI][inI] = '_';
          }
        }
      }

              // BREAK
      // ATTEMPT AT COLOR

      System.out.println("___1_2_3___");
      System.out.println("1 |" + Board[0][0] + "|" + Board[0][1] + "|" + Board[0][2] + "| 1");
      System.out.println("2 |" + Board[1][0] + "|" + Board[1][1] + "|" + Board[1][2] + "| 2");
      System.out.println("3 |" + Board[2][0] + "|" + Board[2][1] + "|" + Board[2][2] + "| 3");
      System.out.println("___1_2_3___\n");

      boolean facs = false;
      while (facs == false){
        System.out.print(playerTurn + " player enter the row and column (in order) of where you want to place your piece (ex. 11 = row: 1, column: 1): ");
        placeRow = scan.nextInt();
        if (!(placeRow == 11 || placeRow == 12 || placeRow == 13 || placeRow == 21 || placeRow == 22 || placeRow == 23 || placeRow == 31 || placeRow == 32 || placeRow == 33)){
          continue;
        }
        placeColumn = placeRow % 10;
        placeRow /= 10;
        if (Board[placeRow-1][placeColumn-1] == '_'){
          facs = true;
          Board[placeRow-1][placeColumn-1] = playerTurn;
          if (playerTurn == 'X'){
            playerTurn = 'O';
          }
          else if (playerTurn == 'O'){
            playerTurn = 'X';
          }
          else{
            System.out.println("\tFatal Error: char playerTurn == !X || !O");
          }
        }
        else{
          continue;
        }
      }
      if (Board[0][0] == 'X' && Board[1][0] == 'X' && Board[2][0] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][1] == 'X' && Board[1][1] == 'X' && Board[2][1] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][2] == 'X' && Board[1][2] == 'X' && Board[2][2] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][0] == 'X' && Board[0][1] == 'X' && Board[0][2] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[1][0] == 'X' && Board[1][1] == 'X' && Board[1][2] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[2][0] == 'X' && Board[2][1] == 'X' && Board[2][2] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][0] == 'X' && Board[1][1] == 'X' && Board[2][2] == 'X'){
        WINNER = 'X';
        break;
      }
      else if (Board[2][0] == 'X' && Board[1][1] == 'X' && Board[0][2] == 'X'){
        WINNER = 'X';
        break;
      }
              // BREAK
      else if (Board[0][0] == 'O' && Board[1][0] == 'O' && Board[2][0] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][1] == 'O' && Board[1][1] == 'O' && Board[2][1] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][2] == 'O' && Board[1][2] == 'O' && Board[2][2] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][0] == 'O' && Board[0][1] == 'O' && Board[0][2] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[1][0] == 'O' && Board[1][1] == 'O' && Board[1][2] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[2][0] == 'O' && Board[2][1] == 'O' && Board[2][2] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[0][0] == 'O' && Board[1][1] == 'O' && Board[2][2] == 'O'){
        WINNER = 'X';
        break;
      }
      else if (Board[2][0] == 'O' && Board[1][1] == 'O' && Board[0][2] == 'O'){
        WINNER = 'X';
        break;
      }
    }

    System.out.println("___1_2_3___");
    System.out.println("1 |" + Board[0][0] + "|" + Board[0][1] + "|" + Board[0][2] + "| 1");
    System.out.println("2 |" + Board[1][0] + "|" + Board[1][1] + "|" + Board[1][2] + "| 2");
    System.out.println("3 |" + Board[2][0] + "|" + Board[2][1] + "|" + Board[2][2] + "| 3");
    System.out.println("___1_2_3___\n");

    if (WINNER == 'X'){
      System.out.println("\t\t\tX PLAYER WINS!\n");
    }else if (WINNER == 'O'){
      System.out.println("\t\t\tO PLAYER WINS!\n");
    }else{
      System.out.println("\t\t\tIT'S A DRAW!\n");
    }
    scan.close();
  }


}